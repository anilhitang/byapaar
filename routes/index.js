var express = require('express');
var router = express.Router();
var Product = require('../models/product');

//var getDBInstance = require('../helpers/db_connections');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Byapaar' });
});

// /* GET login page. */
// router.get('/user/login', function(req, res, next) {
//     res.render('user/login');
// });


// get productsList
router.get('/productsList',  function(req, res, next){
  // getDBInstance.then(function (db) {
  //   var cursor = db.collection("products").find();
  //   cursor.each(function(err, doc) {
  //      console.log(doc);
  //      resultArray.push(doc);
  //  });
  //  db.close();
  // });
    var resultArray = [];
    Product.find({}, function (err, result) {

        if(err) return console.err(err);
            resultArray.push(result);
        res.render('index', { items: resultArray});
    });
});


//insert
router.get('/insert', function(req, res, next){
  getDBInstance.then(function (db) {
    db.collection("products").insertOne({"productName":"Industry tape","price":"35", "productImage":
        "https://images-na.ssl-images-amazon.com/images/I/41PfrO0bHUL._SL500_AC_SS350_.jpg"}, function(err, result) {


    });
    db.close();
  });
  res.render('index', {success: "New data is inserted"});
});



/* GET Hello World page. */
router.get('/helloworld', function(req, res) {
    res.render('helloworld', { newTitle: 'Hello, World!' });
});

module.exports = router;
