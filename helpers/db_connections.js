const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
var db;
// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'byapaar';

function getDb() {
    return MongoClient.connect(url).then(function (client) {
        return db = client.db(dbName);
    })

}

module.exports = getDb();
